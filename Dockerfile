FROM registry.gitlab.com/competitions4/sciroc/dockers/sciroc:1.5

LABEL maintainer="Daniel López Puig <daniel.lopez@pal-robotics.com>"

ARG REPO_WS=/ws
RUN mkdir -p $REPO_WS/src
WORKDIR /home/user/$REPO_WS

# TODO: Put inside ./ws your ROS packges
COPY ./ws /home/user/ws/src

# TODO: add here the debians you need to install
#RUN apt install -y ros-melodic-<pkg_name> pal-ferrum-<pkg_name> <apt-pkg>
RUN apt install -y espeak python-pip pocketsphinx python-sphinxbase python-pocketsphinx cmake swig alsa-base pulseaudio libpulse-dev portaudio19-dev
RUN python -m pip install pyttsx3
RUN pip install --upgrade pocketsphinx
RUN wget "https://raw.githubusercontent.com/Pi-31415/yolo-personal/main/requirements.txt"
RUN sudo apt install -y python3-pip
RUN sudo -H pip3 install --upgrade pip
RUN sudo -H pip install --upgrade pip
RUN pip3 install -r requirements.txt

# Build and source your ros packages
RUN bash -c "source /opt/pal/ferrum/setup.bash \
    && catkin build \
    && echo 'source /opt/pal/ferrum/setup.bash' >> ~/.bashrc \
    && echo 'source /home/user/ws/devel/setup.bash' >> ~/.bashrc"

ENTRYPOINT ["bash"]
