
#!/usr/bin/python

import os,rospkg
from pocketsphinx import LiveSpeech, get_model_path
import pyttsx3
import rospy
import numpy as np
import message_filters
from sensor_msgs.msg import Image
from collections import defaultdict
from std_msgs.msg import String
from pal_zoi_detector.msg import CurrentZoI


engine = pyttsx3.init('espeak')

def speak(audio):
    
    
    engine.setProperty('rate', 125)
    engine.say(audio)
    engine.runAndWait()

rp = rospkg.RosPack()

model_path = get_model_path()

# Updated zones
def callback_zone(data):
    current_table = data.zois[0]
    # Update the table's area in the parameter server
    # Update the number of people in the parameter server
    rospy.set_param('/current_table', current_table)

def callback_check(data):

# Call the current table area
    rospy.loginfo("I see %s", data.data)
    count_objects_result = data
    current_table = rospy.get_param('/current_table')
    print (current_table)
    tables = rospy.get_param('/tables')

    person_count = count_objects_result.data.count('human')
    o = count_objects_result.data.replace('human','')
    orders = o.replace('\n',',')

# calculate and output result
    print ( 'Detected human: num of person', person_count)
    if len(count_objects_result.data) == 6*person_count:
      object_count = 0
      print ( 'No objects')
    else :
      object_count = 1 
      print ( 'Detected objects: ordered',orders)

    if person_count and object_count:
      status = 'Already served'
      speak('current_table')
      speak('Is already serverd')
    elif person_count and not object_count:
      status = 'Needs serving'
      speak('current_table')
      speak('Needs servering')
    elif not person_count and object_count:
      status = 'Needs cleaning'
      speak('current_table')
      speak('Needs cleaning')
    else:
      status = 'Ready'
      speak('current_table')
      speak('Is ready')
    print('Table_status',status)
    # Update the status of the table in the parameter server
    # Update the number of people in the parameter server
    rospy.set_param('/tables/' + current_table + '/person_count', person_count)
    rospy.set_param('/tables/' + current_table + '/status', status)
    rospy.set_param('/tables/' + current_table + '/ordered', orders)
    rospy.loginfo('Updated the status successfully')
    # output status
    print('Status of {0} is {1}'. format(current_table, status))


## Subscirbe msg from YOLO5 detection
def tablecheck():
   
    rospy.init_node('countPeople', anonymous=True)
    rospy.Subscriber('current_zone_of_interest', CurrentZoI, callback_zone)
    rospy.Subscriber("object_detector", String, callback_check)
    rospy.spin()


if __name__== '__main__':
    tablecheck()

