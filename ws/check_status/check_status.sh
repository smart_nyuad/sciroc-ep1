# After object detection, check the table status and update to the 'table_status_update.yaml'
# Load "current_table" and update "table status"
cd /home/user/ws/src/check_status/config
rosparam load table_status_initialize.yaml 
cd /home/user/ws/src/check_status/config 
rosparam load table_status_update.yaml

# Update the current table based on the Zone detector
# Check table status based on object detecion results
cd /home/user/ws/src/check_status/src
python table_check.py

#Save the results
cd /home/user/ws/src/check_status/config
rosparam dump table_status_initialize.yaml 
cd /home/user/ws/src/check_status/config 
rosparam dump table_status_update.yaml


