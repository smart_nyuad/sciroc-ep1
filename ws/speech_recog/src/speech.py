#!/usr/bin/env python

import os,rospkg
import sys
from speech_recog.srv import *
from pocketsphinx import LiveSpeech, get_model_path

import rospy
from std_msgs.msg import String


import pyttsx3

global get_order_bool
get_order_bool = False


keywords_dict={
	'generic-beer':'beer',
	'cocacola-can':'cocacola',
	'plastic-cup':'plastic_cup',
	'tropical-juice-slim-can':'bifrutas_tropical_can',
	'biscuit-pack':'biscuits_pack',
	'sprite-can':'sprite',
	'pringles-chips-red-package':'pringles1',
	'pringles-chips-green-package':'pringles2',
	'italian-team-can-estatay':'estathe',
	'italian-beer-peroni':'peroni',
	'tea-box':'tea_box',
	'fanta-can':'fanta'}



#test2

engine = pyttsx3.init('espeak')

def speak(audio):


    engine.setProperty('rate', 125)
    engine.say(audio)
    engine.runAndWait()

rp = rospkg.RosPack()

model_path = get_model_path()

def getkeywords():

	speech = LiveSpeech(
	    verbose=False,
	    sampling_rate=16000,
	    buffer_size=2048,
	    no_search=False,
	    full_utt=False,
	    hmm=os.path.join(model_path, 'en-us'),
	    lm=os.path.join(rp.get_path('speech_recog'),'src','1885.lm'),
	    dic=os.path.join(rp.get_path('speech_recog'),'src','1885.dic')

	)

	for phrase in speech:
	    return (str(phrase).lower())


def getconfirmation():
	speech = LiveSpeech(
	    verbose=False,
	    sampling_rate=16000,
	    buffer_size=2048,
	    no_search=False,
	    full_utt=False,
	    hmm=os.path.join(model_path, 'en-us'),
	    lm=os.path.join(rp.get_path('speech_recog'),'src','9759.lm'),
	    dic=os.path.join(rp.get_path('speech_recog'),'src','9759.dic')

	)

	for phrase in speech:
	    return (str(phrase).lower())




def validate():

	confirmation = False

	while confirmation == False:

		speak('Please Say your order of three items')

		validated_keywords=[]

		while len(validated_keywords) < 3:

			detected_keywords = (getkeywords().split())

			for keyword in detected_keywords:
				if len(validated_keywords)<3:

					validated_keywords.append(keyword)


		speak('Your order is')
		speak(validated_keywords)

		confirmation = confirm()
	speak('Thank you, your order is confirmed')

	labels=[]
	for keyword in validated_keywords:
		labels.append(keywords_dict[keyword])

	return ' '.join(labels)


def confirm():

	speak('Is that correct?')
	decision = (getconfirmation().split())

	for item in decision:
		if item == 'yes':
			confirm = True
		else:
			confirm = False
			speak('Please repeat')
	return confirm


def publisher():

	order_again = True
	global msg_to_publish
	global pub
	global rate


	speak('Welcome to our cafe')

	string_to_publish = validate()


	msg_to_publish.data= string_to_publish

	pub.publish(msg_to_publish)


	rospy.loginfo(string_to_publish)

	rate.sleep()

            # rospy.set_param("/order_taken", True)

def get_order_srv(req):

    get_order()
    return getOrderResponse(True, "Order taken")

def get_order():
    global get_order_bool
    # print("This is a test")
    get_order_bool = True

def main(args):
    rospy.init_node('speech', anonymous=True)
    global get_order_bool
    global msg_to_publish
    global pub
    global rate

    pub = rospy.Publisher('speech_order', String, queue_size=10)
    rate = rospy.Rate(1)
    msg_to_publish = String()

    s = rospy.Service('/speech_recog/get_order', getOrder, get_order_srv)

    while not rospy.is_shutdown():
        if get_order_bool == True:
            print("(Inside speech) Taking order")
            publisher()
            rospy.set_param("/order_taken", True)
            get_order_bool = False
        # print(get_order_bool)
        rate.sleep()



if __name__ == '__main__':
    try:
        main(sys.argv)
    except rospy.ROSInterruptException:
        pass
