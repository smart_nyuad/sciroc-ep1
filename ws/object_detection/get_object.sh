# Before running this script, run the command below to get the images
# rosrun image_view image_saver image:="/xtion/rgb/image_rect_color"
#
# This script performs object detection on images from Tiago's camera, and
# publishes the results to the 'object_detector' topic

# Delete previous txt files from previous detection
# cd /home/user/ws/src/main_test/temp
# find . -type f -iname \*.txt -delete
# find . -type f -iname \*.jpg -delete
# find . -type f -iname \*.ini -delete
# Detect objects
cd /home/user/ws/src/object_detection/scripts/
python3 robovision.py
# cd /home/user/ws/src/main_test/temp
# rosrun object_detection detect.py
# # Delete jpg,ini files from previous detection
# cd /home/user/ws
