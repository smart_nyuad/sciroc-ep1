#!/usr/bin/env python

## This node publishes the detected objects in std_msgs/Strings messages
## to the 'object_detector' topic

# You don't have to run this node directly. First go to /home/user/ws. Then run the following.
# rosrun image_view image_saver image:="/xtion/rgb/image_rect_color"
# /home/user/ws/src/object_detection/get_object.sh

import rospy
from std_msgs.msg import String
from object_detection.srv import *
import os

global detect_bool
detect_bool = False

def detect():
    global image_path
    largest_image_name = ""
    base_directory = "/home/user/ws/src/main_node/temp"
    pub = rospy.Publisher('object_detector', String, queue_size=10)
    file_list = os.listdir(base_directory)
    for image in file_list:
        if ".jpg" in image:
            image_name = image
            #get the number ID of the image
            image_id = image_name.replace("left", "")
            image_id = image_id.replace(".jpg", "")
            image_id = int(image_id)
            if image_id > largest_id:
                largest_id = image_id
                largest_image_name = image_name

    image_path = base_directory + "/" + largest_image_name
    f = open(image_path.replace(".jpg",".txt"), "r")
    data = f.read()
    rospy.loginfo(data)
    pub.publish(data)

def detect_test():
    global detect_bool
    detect_bool = True

def detect_srv(req):
    detect_test()
    return detectObjectsResponse(True, "Image taken")

def main():

    global detect_bool
    rospy.init_node('detect', anonymous=True)
    rate = rospy.Rate(1)

    pub = rospy.Publisher('object_detector', String, queue_size=10)

    largest_image_name = "0000.jpg"
    base_directory = "/home/user/ws/src/main_node/temp"
    largest_id = 0
    # image_path = "/home/user/ws/src/main_node/temp/0000.jpg"


    s = rospy.Service('/object_detection/detect_Objects', detectObjects, detect_srv)
    while not rospy.is_shutdown():
        if detect_bool == True:
            # print("Object detection test")
            os.system("rosservice call /image_saver/save")
            os.system("/home/user/ws/src/object_detection/get_object.sh")

            file_list = os.listdir(base_directory)
            for image in file_list:
                if ".jpg" in image:
                    image_name = image
                    #get the number ID of the image
                    image_id = image_name.replace("left", "")
                    image_id = image_id.replace(".jpg", "")
                    image_id = int(image_id)
                    if image_id > largest_id:
                        largest_id = image_id
                        largest_image_name = image_name

            image_path = base_directory + "/" + largest_image_name
            print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
            print(image_path)

            f = open(image_path.replace(".jpg",".txt"), "r")
            data = f.read()
            rospy.loginfo(data)
            pub.publish(data)
            detect_bool = False
        rate.sleep()

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
