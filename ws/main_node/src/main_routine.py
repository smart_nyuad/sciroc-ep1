#!/usr/bin/env python

import os,rospkg, rospy, actionlib, pyttsx3, time, sys

from speech_recog.srv import *
from pocketsphinx import LiveSpeech, get_model_path
from control_msgs.msg import PointHeadActionGoal
from pal_navigation_msgs.msg import GoToPOIAction, GoToPOIActionGoal
from actionlib_msgs.msg import GoalStatusArray, GoalStatus
from std_msgs.msg import String, Duration
from geometry_msgs.msg import PointStamped, Vector3
from object_detection.srv import*
from pal_zoi_detector.msg import CurrentZoI
from sciroc_ep1_object_manager.srv import*
from main_node.srv import*

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# FUNCTIONS

engine = pyttsx3.init('espeak')
def speak(audio):


    engine.setProperty('rate', 125)
    engine.say(audio)
    engine.runAndWait()

# Function to evaluate the status of the tables and get the tables that need serving.
def who_needs_serving():
    global stage
    global tables_to_be_served
    global number_of_tables
    global table_serving
    tables_to_be_served_aux = []

    for i in range(1,number_of_tables + 1):
        param_aux = '/tables/table' + str(i) + '/status'
        # order_aux = '/tables/table' + str(i) + '/ordered'
        status_aux = rospy.get_param(param_aux)
        # order_aux = rospy.get_param(order_aux)
        if status_aux == "Needs serving":
            table_serving = "table" + str(i)
            tables_to_be_served[table_serving] = "pending"

    if tables_to_be_served.keys() != []:
        rospy.loginfo("The following tables need to be served:")
        rospy.loginfo(tables_to_be_served.keys())
        stage = "taking_orders"
    else:
        rospy.loginfo("There are no tables that need to be served")
        speak("There are no tables that need to be served")
        stage = "all_served"

def start_process(req):
    global current_order
    current_order = ''
    global count
    count = 1
    global tables_to_be_served
    tables_to_be_served = dict()
    global table_serving_idx
    table_serving_idx = 0
    global table_serving
    table_serving = ''
    os.system("/home/user/ws/src/main_node/scripts/load_params.sh")


    goal_pub.publish(goal_msg)
    speak("Welcome everyone, I will check who needs to order")
    return startProcessResponse(True, "Starting process")

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Callback to check the status of the navigation (status = 3 means the robot got to destination)
def status_cb(data):

    # Variable declaration. We make them global.
    global stage
    global count
    global current_order
    global current_status
    global tables_to_be_served
    global table_serving_idx
    global table_serving
    global detect_srv
    global take_order_srv
    global order_objects_srv
    global move_items_on_table_srv
    global point_head_msg
    global point_head_pub
    global number_of_tables
    global goal_msg

    # We get the current status of the navigation. TODO: Implement a failsafe in case the goal fails for some reason.
    # Status = 0 (Pending)
    # Status = 1 (Active)
    # Status = 2 (Preempted)
    # Status = 3 (Succeeded) <-- We care about this one.
    # Status = 4 (Aborted)
    if data.status_list != []:
        current_status = data.status_list[0].status

        # If we are 'inspecting' and status = 3, means we arrived to the next table to inspect.
        if stage == "inspecting":

            if current_status == 3 and str(count)==data.status_list[0].goal_id.id:

                # We point the head towards the table.
                point_head_pub.publish(point_head_msg)
                time.sleep(1.5)
                detect_srv() # We call the service that's going to take a picture and process it.
                time.sleep(1.5)

                count += 1
                if count == number_of_tables + 1: # Means we inspected all the tables
                    stage = "who_needs_serving"
                    count = 0
                else:
                    current_table = "table"+str(count)
                    goal_msg.goal.poi.data = current_table
                    goal_msg.goal_id.id = str(count)
                    rospy.logdebug("Goal %s has been received", current_table)
                    goal_pub.publish(goal_msg)

        # If we are 'taking_orders' and status = 3, means we arrived to a table that needs to be served.
        if stage == "taking_orders":

            table_serving = tables_to_be_served.keys()[table_serving_idx]
            if current_status == 3 and table_serving==data.status_list[0].goal_id.id:

                rospy.loginfo("Taking order for %s", table_serving)
                take_order_srv() # <-- Calling the service that activates the speech recognition.
                # We need to 'pause' the algorithm at this point until the order is finished.
                while (rospy.get_param("/order_taken") == False):
                    rospy.logdebug("Taking an order")
                    # time.sleep(2)

                table_serving_idx += 1

                # Did we already check all the tables that need serving?
                if table_serving_idx == len(tables_to_be_served.keys()):
                    stage = "getting_orders"
                    table_serving_idx = 0
                else:
                    table_serving = tables_to_be_served.keys()[table_serving_idx]
                    rospy.loginfo("Taking order for %s", table_serving)
                    current_table = table_serving
                    goal_msg.goal.poi.data = current_table
                    goal_msg.goal_id.id = current_table
                    rospy.logdebug("Goal %s has been received", current_table)
                    goal_pub.publish(goal_msg)
                    rospy.set_param("/order_taken", False) # <-- We reset /order_taken.

        # If we are in the stage 'check_order' and status = 3, means we arrived to the kitchen.
        if stage == "check_order":

            table_serving = tables_to_be_served.keys()[table_serving_idx]
            if current_status == 3 and "kitchen"==data.status_list[0].goal_id.id:

                current_order = tables_to_be_served[table_serving]
                speak_str = "The " + table_serving + "ordered " + current_order[0] + "," + current_order[1] + "," + current_order[2]
                speak(speak_str)
                rospy.loginfo("Ordering the objects %s, %s, %s for %s", current_order[0], current_order[1], current_order[2], table_serving)
                order_objects_srv(current_order[0], current_order[1], current_order[2])
                stage = "check_order_2" # <-- We change so we don't keep ordering infinite items.
                time.sleep(1) # <-- We give some time for the items to spawn.
                # Pointing the head towards the table in order to recognise the objects. Current point (1.9, -0.5, 0.8).
                point_head_msg.goal.target.point.y = -0.5
                point_head_msg.goal.target.point.z = 0.8
                point_head_pub.publish(point_head_msg)
                time.sleep(1.5)
                detect_srv() # <-- Same as before, we call the service to take an image and detect the objects.
                time.sleep(1.5)

        # If the stage is 'placing_on_table' and status = 3, means we arrived to the table that made an order.
        if stage == "placing_on_table":

            if current_status == 3 and (table_serving + "serv")==data.status_list[0].goal_id.id:

                speak("Sorry for the delay, here is your order")
                time.sleep(1)
                move_items_on_table_srv()

                # Did we serve all the tables?
                if table_serving_idx < len(tables_to_be_served.keys())-1:
                    table_serving_idx += 1
                    stage = "getting_orders"
                else:
                    stage = "all_served"

        if stage == "finished":

            if current_status == 3 and "patrol1" == data.status_list[0].goal_id.id:

                # We set everything ready to start again.
                goal_msg.goal.poi.data = "table1"
                goal_msg.goal_id.id = "1"
                stage = "inspecting"


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Callback for the topic getting the order from the speech recognition.
def speech_cb(data):

    global tables_to_be_served
    global current_order
    global table_serving_idx
    global table_serving

    current_order = data.data
    current_order = current_order.split()
    tables_to_be_served[table_serving] = current_order
    rospy.loginfo(tables_to_be_served)


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Callback to get in which zone the robot is.
def zone_cb(data):

    if data.zois != []:
        current_table = data.zois[0]
        rospy.set_param('/current_table', current_table) # TODO: Turn this into a global variable?


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Callback for whenever we receive data from the object recognition node.
def check_cb(data):

    global current_order
    global stage
    global swap_objects_srv
    global move_items_on_tray_srv
    right = current_order

    # If we receive data from the object recognition topic and the stage is 'check_order_2', we are at the kitchen analysing the order that we got.
    if stage == "check_order_2":

        wrong = data.data;
        wrong = wrong.replace('\n', ',')
        wrong = wrong[:-1]
        wrong = wrong.split(',') # <-- We convert the data to a list of strings.
        rospy.loginfo("Is this the correct order? %s", wrong)


        # We check for the objet that is not present in the order from the kitchen.
        for i in range(3):
            a = right.count(right[i])
            b = wrong.count(right[i])
            if a != b:
                rospy.loginfo("The item %s is missing", right[i])
                missing = right[i]
                break

        # We check for the object that needs to be replaced.
        for j in range(3):
            a = wrong.count(wrong[j])
            b = right.count(wrong[j])
            if a != b:
                rospy.loginfo("The item %s needs to be replaced", wrong[j])
                to_replace = wrong[j]
                break

        speak_str = "Excuse me, I think you made a mistake. The " + to_replace + "needs to be replaced with a" + missing
        speak(speak_str)
        time.sleep(4)
        swap_objects_srv(to_replace, missing)
        time.sleep(1)
        move_items_on_tray_srv()
        stage = "delivering"

    # The rest of the callbacks to this function will happen while the tables are being inspected (Stage 1)
    else:
        rospy.loginfo("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        rospy.loginfo("The table contains: %s", data.data)
        count_objects_result = data
        current_table = rospy.get_param('/current_table')
        tables = rospy.get_param('/tables')
        person_count = count_objects_result.data.count('human')
        o = count_objects_result.data.replace('human', '')
        orders = o.replace('\n',',')

        rospy.loginfo("Detected %i humans", person_count)
        if len(count_objects_result.data) == 6*person_count:
            object_count = 0
            rospy.loginfo("No objects detected on the table")
        else:
            object_count = 1
            rospy.loginfo("Detected the objects: %s", orders)

        if person_count and object_count:
            status = 'Already served'
        elif person_count and not object_count:
            status = 'Needs serving'
        elif not person_count and object_count: # TODO: Check rules, wouldn't be this case 'ready'? 'Needs cleaning' would be no people and objects.
            status = 'Needs cleaning'
        else:
            status = 'Ready'

        rospy.loginfo("The status for %s is %s",current_table, status)
        speech_string = str(current_table) + str(status)
        speak(speech_string)
        rospy.loginfo("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        if person_count > 1:
            speech_string2 = str(person_count)+'persons'
            speak(speech_string2)
        elif person_count == 1:
            speech_string2 = str(person_count)+'person'
            speak(speech_string2)




        rospy.set_param('/tables/' + current_table + '/person_count', person_count)
        rospy.set_param('/tables/' + current_table + '/status', status)
        rospy.set_param('/tables/' + current_table + '/ordered', orders)

    	f1 = open("/home/user/ws/src/main_node/status_output.txt","a")
    	L0 = ["~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~", "\n"]
    	L1 = ["The status for ", current_table, " is ", status, "\n"]
    	L2 = ["Number of customers for ", current_table, " is ", str(person_count), "\n"]
    	f1.writelines(L0)
    	f1.writelines(L1)
    	f1.writelines(L2)
    	f1.writelines(L0)
    	f1.close()

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


def main(args):

    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # VARIABLES AND INITIALIZATION
    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    global current_order # Contains the active order
    current_order = ''
    global count # Counter to keep track of the tables inspected
    count = 1
    global current_status # Informs about the status of the navigation
    current_status = 0
    global stage # Keeps track of the current stage
    stage = 'inspecting' # We initialize the variable to the first stage.
    global tables_to_be_served # Contains the list of tables to be served
    tables_to_be_served = dict()
    global table_serving_idx # Current table being served
    table_serving_idx = 0
    global table_serving
    table_serving = ''
    global number_of_tables
    number_of_tables = 6
    # global start_bool
    # start_bool = False

    # Messages
    global goal_msg
    global point_head_msg
    # Publishers
    global goal_pub
    global point_head_pub
    # Services
    global detect_srv
    global take_order_srv
    global order_objects_srv
    global move_items_on_tray_srv
    global move_items_on_table_srv
    global swap_objects_srv
    global start_process_srv

    rospy.init_node('main_routine', anonymous=True, log_level=rospy.INFO) # Change the log_level in accordance (rospy.DEBUG or rospy.INFO)

    rate = rospy.Rate(1)
    rospy.set_param("/order_taken", False)
    os.system("/home/user/ws/src/main_node/scripts/load_params.sh")

    # Publishers definition
    goal_pub = rospy.Publisher("/poi_navigation_server/go_to_poi/goal", GoToPOIActionGoal, queue_size = 1)
    point_head_pub = rospy.Publisher("/head_controller/point_head_action/goal", PointHeadActionGoal, queue_size = 1)
    # Subscribers definition. They all point to the proper callback function
    rospy.Subscriber("/poi_navigation_server/go_to_poi/status", GoalStatusArray, status_cb)
    rospy.Subscriber("/object_detector", String, check_cb)
    rospy.Subscriber("/current_zone_of_interest", CurrentZoI, zone_cb)
    rospy.Subscriber("/speech_order", String, speech_cb)
    # Services definition
    detect_srv = rospy.ServiceProxy('/object_detection/detect_Objects', detectObjects)
    take_order_srv = rospy.ServiceProxy('/speech_recog/get_order', getOrder)
    order_objects_srv = rospy.ServiceProxy("/sciroc_object_manager/get_three_ordered_items", GetThreeOrderedItems)
    swap_objects_srv = rospy.ServiceProxy("/sciroc_object_manager/change_the_item", ChangeTheItem)
    move_items_on_tray_srv = rospy.ServiceProxy("/sciroc_object_manager/move_items_on_the_tray", MoveItemsOnTheTray)
    move_items_on_table_srv = rospy.ServiceProxy("/sciroc_object_manager/move_items_on_the_closest_table", MoveItemsOnClosestTable)
    start_process_srv = rospy.Service("/start_process", startProcess, start_process)

    time.sleep(2) # Let's give some time for everything to properly load.

    # Define the message for navigating to the tables.
    goal_msg = GoToPOIActionGoal()
    goal_msg.goal.poi.data = "table1"
    goal_msg.goal_id.id = "1"

    # Define the message to point the head of the robot.
    # Current point (1.9, 0, 0.5) is fixed for all tables, this could change depending on the table.
    point_head_msg = PointHeadActionGoal()
    point_head_msg.goal.target.header.frame_id = "/base_link"
    point_head_msg.goal.target.point.x = 1.9
    point_head_msg.goal.target.point.y = 0
    point_head_msg.goal.target.point.z = 0.5
    point_head_msg.goal.pointing_axis.x = 0
    point_head_msg.goal.pointing_axis.y = 0
    point_head_msg.goal.pointing_axis.z = 1
    point_head_msg.goal.pointing_frame = "xtion_rgb_optical_frame"

    # We wait until the publisher for the navigation goal is available, just in case.
    while goal_pub.get_num_connections()==0:
        rospy.loginfo("Waiting for publisher...")

    rospy.wait_for_service('/start_process')

    # Everything starts with the robot moving towards the first table.
    # goal_pub.publish(goal_msg)


    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # MAIN LOOP
    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    while not rospy.is_shutdown():

        rospy.logdebug("DEBUG: Current_stage = %s, current_status = %s, current_order = %s, count = %i", stage, current_status, current_order, count)

        # Stage 1: The inspection of the tables happens in the callback for the result navigation function 'status_cb()'

        # Stage 2: All the tables have been inspected, let's get those that needs serving.
        if stage == "who_needs_serving":
            time.sleep(5) # <-- Some time is needed for the last picture taken to be processed by the object recognition algorithm.
            who_needs_serving()
            rospy.loginfo("On my way to the first table that needs ordering!")
            goal_msg.goal.poi.data = tables_to_be_served.keys()[table_serving_idx]
            goal_msg.goal_id.id = tables_to_be_served.keys()[table_serving_idx]
            goal_pub.publish(goal_msg)

        # Stage 3: All the tables have been attended. Time to head to the kitchen to get the first order.
        if stage == "getting_orders":
            rospy.loginfo("On my way to the kitchen!")
            goal_msg.goal.poi.data = "kitchen"
            goal_msg.goal_id.id = "kitchen"
            goal_pub.publish(goal_msg)
            stage = "check_order" # We change the stage so we don't keep sending the 'kitchen' goal over and over.
            # print("Debug 2")

        # Stage 4: Ordering the items to the kitchen and checking if they are the proper items. It happens in the 'check_cb()' function.

        # Stage 5: Now that the robot is carrying the items, we deliver them to the proper table.
        if stage == "delivering":
            table_aux = table_serving + "serv" # <-- We have two waypoints for each table. One further away from the table for inspecting, and one closer for delivering.
            rospy.loginfo("Delivering the order %s to the %s", current_order, table_serving)
            goal_msg.goal.poi.data = table_aux
            goal_msg.goal_id.id = table_aux
            goal_pub.publish(goal_msg)
            time.sleep(1) #
            stage = "placing_on_table"

        # Stage 6: The robot arrives to the table and delivers the objects. It happens in the 'status_cb()' function.

        if stage == "all_served":
            rospy.loginfo("All tables have been served")
            goal_msg.goal.poi.data = "patrol1"
            goal_msg.goal_id.id = "patrol1"
            goal_pub.publish(goal_msg)
            stage = "finished"
            start_bool = False

        rate.sleep()

if __name__ == '__main__':
    try:
        main(sys.argv)
    except rospy.ROSInterruptException:
        pass
