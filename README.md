** S.M.A.R.T. Team. SciRoc 2021 Episode 1 solution. **

**Setup on host**

For the speech recognition:

1. Open PulseAudio Preferences (if not installed do **apt-get install paprefs**) > Network Server > Check Enable network access to local sound devices.
2. Restart your system

Now the docker should be able to stream to and receive audio from the host through the TCP server, removing the need to figure out resource mapping and allocation. I'm not sure how this will be on the robotic platform but for now this is ideal for testing.

**To test the node (inside the docker)**

1. Make sure you have the latest version of the image from the competition's gitlab (as of right now the latest version is 1.5)
2. Download this branch and build it, based on the 1.5 image.
3. Launch the docker with the script from PAL. In order to share the workspace folder you can do so as such:
`./<path_to_script>/pal_docker.sh -it -v /dev/snd:/dev/snd -v /$HOME/<path_to_downloaded_branch>/ws:/home/user/ws/src <name_of_image_to_launch>`
4. Once you're inside the docker you can run `terminator -u &` in order to open as many terminals as you need inside the docker.
5. Source the models: `export GAZEBO_MODEL_PATH=$GAZEBO_MODEL_PATH:/home/user/ws/src/Sciroc2EP1-SimulatedEnv/models`
6. In order to launch all the needed nodes, you just need to run in a terminal `roslaunch main_node SMART_team_sciroc_ep1.launch`. This will launch the simulation, the speech_recog node, the image_saver node, the object_detection node and the main_node node.
7. Wait for the gazebo environment to load and for the robot to fully tuck in the arm (a message in the terminal where the launch file was launched will appear as "Arm tucked").
8. Now everything is ready to start the process by calling a service. To do so, in a new terminal, run:
    `rosservice call /start_process`

After that, the robot will start inspecting the tables and getting the status of it. Whenever the robot reaches a table, the head points towards the table and takes a picture of the scene. Once it finishes inspecting all the tables, it will go to each one of the tables that need serving and get an order. The robot will get all the orders first (storing them in a python dictionary) and head to the kitchen to get the first one. Once the robot reaches the kitchen, three objects are spawned, one of them not being the right one. The robot changes the wrong item for the proper one and then heads towards the table to deliver the order. Once the order is delivered, if there are more tables to be served the robot will head to the kitchen to get the next order. Once all tables have been served the robot will move to an initial position to rest. The status of all the tables will be stored in the file `status_output.txt` in the package folder of the `main_node`.

Possible items for object detection are

```
generic beer
cocacola can
plastic cup
tropical juice slim can
biscuit pack
sprite can
pringles chips red package
pringles chips green package
Italian team can estathé
Italian beer peroni
tea box
fanta can
```
